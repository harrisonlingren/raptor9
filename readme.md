# Raptor 9 Booster Rocket

This project contains the launch & landing sequence kerboscript for my KSP raptor9
booster, which is available on Steam here:  
https://steamcommunity.com/sharedfiles/filedetails/?id=2124705158


Similar to the real world, the software can be difficult to work with. In its current
form, it is able to launch the booster outside of Kerbin's atmosphere, and land itself 
on the surface - as long as it's flat enough :)

The throttle control on landing is driven by a PID that is modeled and tuned with
multiple iterations of logged flight data (see commit history...). It's pretty stable
as of v0.40 - it can bring itself from ~250 m/s down to a stable 5 m/s in less than 15
seconds. This can obviously be further improved with more iteration and tuning.
