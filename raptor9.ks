// Copyright (c) 2020 Harrison Lingren

// globals
set message to "".
set script_version to "0.40_4".

// HELPER FUNCTIONS
// -----------------------------

// launch sequence
declare function launch {
    sas off. rcs on.
    lock throttle to 1.0.
    lock steering to up.
    set message to "launch sequence started!". log_info().
    from {local countdown is 10.} until countdown = 0 step {set countdown to countdown - 1.} do {
        set message to "T-" + countdown. log_info().
        wait 1.
    }

    set message to "go for launch!". log_info().

    until ship:maxthrust > 0 {
        wait 0.1.
        set message to "". log_info().
        stage.
    }
}

// landing burn sequence
declare function land {
    set message to "landing sequence started". log_info().
    
    set t to 0.0.  set interval to 0.1.
    lock vs to ship:verticalspeed.
    lock throttle to t.
    
    // setup PID loop
    set t_loop to pidloop(2.719,0.001252,0.5).
    set t_loop:maxoutput to 0.1.
    set t_loop:minoutput to -0.1.
    set t_loop:setpoint to -5.

    until alt:radar <= 29 {
        set t_update to t_loop:update(time:seconds, vs).
        set t to max(0, min(1, t + t_update)).

        // log
        set v_log to "> alt: " + round(alt:radar, 2) + " | vs: " + round(ship:verticalspeed, 2) + " | t: " + round(t, 4) + " | dt: " + round(t_update, 4).
        set message to time:seconds + " | " + v_log. log_debug().

        // if ship:verticalspeed > 0 { set t to 0.1. }.

        // interval
        wait interval.
    }    

    lock throttle to 0. lock steering to up.
    set message to "landed!". log_info().
    wait 8. unlock steering. sas on. 
}

// log level info
declare function log_info {
    print message.
    log time:clock + " | " + message to ("raptor9/logs/raptor9/v" + script_version + ".log").
}

// log level debug
declare function log_debug {
        log time:clock + " | " + message to ("raptor9/logs/raptor9/v" + script_version + ".log").
}


// MAIN SEQUENCE
// -----------------------------------------

clearscreen.
set message to "loaded raptor9 sequence v" + script_version. log_info().
set message to "copyright (c) 2020 Harrison Lingren". log_info().
set message to "-----------------------------------------". log_info().
set message to "". log_info().

// launch sequence
launch().

wait until alt:radar >=50.
set message to "retracting landing gear". log_info().
gear off.

// set heading
wait until apoapsis >= 15000.
set message to "steering to heading(-21, 62)". log_info().
lock steering to heading(-21, 62).
// lock steering to heading(1, 89). // DEBUG

// engine shutdown
wait until apoapsis >= 72000.
set message to "target 72k apoapsis, disengaging engine". log_info().
lock throttle to 0.0.
unlock steering.
sas on.

// track retrograde
wait until (alt:radar >= 68000 and ship:verticalspeed < 0).
set message to "tracking retrograde for landing burn". log_info().
set sasmode to "RETROGRADE".

// activate airbrakes and landing gear
wait until alt:radar <= 35000.
set message to "engaging airbrakes". log_info().
brakes on.

wait until alt:radar <= 10000.
sas off. lock steering to up.

wait until alt:radar <= 5000.
set message to "extending landing gear". log_info().
gear on.

wait until alt:radar <= 1550.
land().

print "(...maybe?)".
